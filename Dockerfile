FROM python:3.8

ADD . /djangoapi

WORKDIR /djangoapi

RUN pip install --upgrade pip

RUN pip install -r /djangoapi/requirements.txt

RUN python manage.py makemigrations

RUN python manage.py migrate

RUN python manage.py collectstatic --noinput

CMD gunicorn --bind 0.0.0.0:$PORT DjangoAPI.wsgi