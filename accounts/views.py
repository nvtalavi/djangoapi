from django.shortcuts import render
from rest_framework import viewsets
from .models import Answer
from .serializers import AnswerSerializer

# Create your views here.
class TestView(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer