from .models import Answer
from rest_framework import serializers

class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = ("answer_text", "pub_date")